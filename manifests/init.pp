# == Class: mobi_mediamuxer
#
# installs mobi_mediamuxer
#
# === Parameters:
#
#    $package::      the mobi_mediamuxer package name
#
#    $version::      the mobi_mediamuxer package version
#
#    $livebackend_fragmentserver:: (v)ip or hostname to the livebackend-fragmentserver
#
#    $vodbackend_fragmentserver:: (v)ip or hostname to the vodbackend-fragmentserver
#
#    $recordingbackend_fragmentserver:: (v)ip or hostname to the recordingbackend-fragmentserver
#
#    $licensemanagerserver:: (v)ip or hostname to the licensemanager-server
#
#    $encrypt_hls:: On/Off
#
#    $forwardedit:: On/Off
#
#    $livemanifest_size:: Number of playlist items in Live Manifests  [Default: 10]
#
#    $auto_update:: true/false (Wrapper for ensure => latest, )
#
#    $md3:: true/false (switch for which version of mediamuxer to use, true is the new for mediadelivery3.0 [D: false]
#
#    $serve_live::  Whether or not to serve incoming live requests (D: On)
#
#    $livecontentroot:: Where to look for live content [D: /var/www/dash-live]
#
#    $serve_vod::   Serve incoming vod requests or not. (D: On)
#
#    $vodcontentroot::  where to look for vod content [D: /var/Jukebox/vod]
#
#    $serve_recordings:: Serve incoming recordings requests or not. (D: On)
#
#    $recordingscontentroot:: Where to look for recordings [D: /var/Jukebox/recordings]
#
#    $deploymentserverurl:: Url to the deployment server
#
#    $logfile_location::  Path to mediamuxer log file [D: /var/log/mobi-mediamuxer]
# === Requires
#
#    Apache
#
# === Sample Usage
#
#  class { "mobi_mediamuxer" :
#  }
#
# Remember: No empty lines between comments and class definition
#
class mobi_mediamuxer (
    ###icinga.pp
    $icinga = hiera('mobi_mediamuxer::icinga',$mobi_mediamuxer::params::icinga),
    $icinga_instance = hiera('icinga_instance',$mobi_mediamuxer::params::icinga_instance),
    $icinga_cmd_args = hiera('mobi_mediamuxer::icinga_cmd_args',$mobi_mediamuxer::params::icinga_cmd_args),
    ###end icinga.pp
    $package = hiera('mobi_mediamuxer::package',$mobi_mediamuxer::params::package),
    $version = hiera('mobi_mediamuxer::version',$mobi_mediamuxer::params::version),
    $livebackend_fragmentserver=hiera('mobi_mediamuxer::livebackend_fragmentserver',$mobi_mediamuxer::params::livebackend_fragmentserver),
    $vodbackend_fragmentserver=hiera('mobi_mediamuxer::vodbackend_fragmentserver',$mobi_mediamuxer::params::vodbackend_fragmentserver),
    $recordingbackend_fragmentserver=hiera('mobi_mediamuxer::recordingbackend_fragmentserver',$mobi_mediamuxer::params::recordingbackend_fragmentserver),
    $licensemanagerserver=hiera('mobi_mediamuxer::licensemanagerserver',$mobi_mediamuxer::params::licensemanagerserver),
    ###config parameters for mediamuxer version > 0.8.0
    $md3=hiera('mobi_mediamuxer::md3',$mobi_mediamuxer::params::md3),
    $deployment_config_path=hiera('mobi_mediamuxer::deployment_config_path',$mobi_mediamuxer::params::deployment_config_path),
    $deployment_server_url=hiera('mobi_mediamuxer::deployment_server_url',$mobi_mediamuxer::params::deployment_server_url),
    $drmproxy_url=hiera('mobi_mediamuxer::drmproxy_url',$mobi_mediamuxer::params::drmproxy_url),
    $vod_root_paths=hiera('mobi_mediamuxer::vod_root_paths',$mobi_mediamuxer::params::vod_root_paths),
    $live_root_paths=hiera('mobi_mediamuxer::live_root_paths',$mobi_mediamuxer::params::live_root_paths),
    $catchup_root_paths=hiera('mobi_mediamuxer::catchup_root_paths',$mobi_mediamuxer::params::catchup_root_paths),
    $serve_fmp4=hiera('mobi_mediamuxer::serve_fmp4',$mobi_mediamuxer::params::serve_fmp4),
    $serve_dash=hiera('mobi_mediamuxer::serve_dash',$mobi_mediamuxer::params::serve_dash),
    $serve_hls=hiera('mobi_mediamuxer::serve_hls',$mobi_mediamuxer::params::serve_hls),
    $serve_ss=hiera('mobi_mediamuxer::serve_ss',$mobi_mediamuxer::params::serve_ss),
    $logfile_location=hiera('mobi_mediamuxer::logfile_location',$mobi_mediamuxer::params::logfile_location),
    $static_manifest_max_age=hiera('mobi_mediamuxer::static_manifest_max_age',$mobi_mediamuxer::params::static_manifest_max_age),
    $dynamic_manifest_max_age=hiera('mobi_mediamuxer::dynamic_manifest_max_age',$mobi_mediamuxer::params::dynamic_manifest_max_age),
    $vod_segment_max_age=hiera('mobi_mediamuxer::vod_segment_max_age',$mobi_mediamuxer::params::vod_segment_max_age),
    $live_segment_max_age=hiera('mobi_mediamuxer::live_segment_max_age',$mobi_mediamuxer::params::live_segment_max_age),
    $catchup_segment_max_age=hiera('mobi_mediamuxer::catchup_segment_max_age',$mobi_mediamuxer::params::catchup_segment_max_age),
    $subtitle_max_age=hiera('mobi_mediamuxer::subtitle_max_age',$mobi_mediamuxer::params::subtitle_max_age),
    $log_level=hiera('mobi_mediamuxer::log_level',$mobi_mediamuxer::params::log_level),
    $external_host_mm=hiera('mobi_mediamuxer::external_host_mm',$mobi_mediamuxer::params::external_host_mm),
)
inherits mobi_mediamuxer::params {
    include mobi_mediamuxer::install, mobi_mediamuxer::config

    anchor { "mobi_mediamuxer::begin": } -> Class["mobi_mediamuxer::install"]
    class { "mobi_mediamuxer::icinga":} ->
    Class["mobi_mediamuxer::config"] -> anchor { "mobi_mediamuxer::end": }

    os::motd::register { $name : }
}
